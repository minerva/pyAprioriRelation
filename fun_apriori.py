#!/usr/bin/env python2.7
# -*-encoding:utf8-*-

from fim import apriori

def fun_apriori(iter_dict,biz_dict,*args):
    """
    run apriori. package author Christian Borgelt.
    """
    rlis = apriori(iter_dict.itervalues(),target='a',supp=0,conf=0,zmin=0,zmax=9, report='as',eval='x',thresh=0)

    #adjust output
    rlis = sorted(rlis,key = lambda x:x[1],reverse = True)

    #print rlis,type(rlis) : [((item_id1,item_id2,...),value,support),...] list.

    _str_to_write = ''
    for r in rlis:
        (body,value,supp) = (r[0],r[1],r[2])
        if not body:
            _items_namestr = 'all'
        else:
            _items_namestr = '-'.join([biz_dict[i] for i in body])
        _items_len = len(body)
        _items_idstr = '-'.join(body)

        var_str = '%s|'*len(args)
        args_str = '|'.join([str(i) for i in args])+'|'
        str_local = ('%d|%s|%s|%d|%.4f') % (_items_len,_items_idstr,_items_namestr,value,supp)
        _str_to_write += args_str + str_local + '\n'
    return _str_to_write
