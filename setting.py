# -*- encoding:utf8 -*-

#IQ CONNECTION
IQ_CONNECT = {
        'userid': 'DBA',
        'password': '****',
        'host': '127.0.0.1:4100'
        }

#FILE PATH TO WRITE LIST DATA.
SRCDATA_FILE_PATH = '/xwtec/bin/xw_arearelat_apri/aprioriv02/srcfile.tmp'

#RULES FILE PATH
RULE_FILE_PATH = '/xwtec/bin/xw_arearelat_apri/aprioriv02/rules.tmp'

#COLUMNS OF TABLE TO LOAD.
LOAD_COLUMN_TUPLE = ('f_date','city_id','items_len','items_id','items_name','items_count','items_support')

#TARGET TABLE NAME
LOAD_TARGET_TABLE = 'kyfx_relation_rules'

#FILE SEPARATOR.
LOAD_FIELD_SEPARATOR = '|'

#SRCFILE LINE TERMINATOR
LOAD_LINE_TERMINATOR = '0x0a'
